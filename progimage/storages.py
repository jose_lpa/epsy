import fnmatch
import io
import mimetypes
import os
import uuid
from typing import BinaryIO, Generator, Tuple

from PIL import Image

from progimage import config


class MetaStorage(type):
    """
    Metaclass for `Storage` to enforce all subclasses to follow defined
    patterns for attributes.
    """
    def __new__(mcs, name: str, bases: tuple, class_dict: dict):
        base_uri = class_dict.get('BASE_URI')
        if base_uri is None and name != 'Storage':
            raise TypeError('Mandatory setting `STORAGE_BASE_URI` missing.')

        return type.__new__(mcs, name, bases, class_dict)


class Storage(metaclass=MetaStorage):
    """ Base class for storage backend implementation. """
    CHUNK_SIZE_BYTES = 4096
    BASE_URI = None

    def store_image(self, image_stream: io.BytesIO, file_name: str) -> None:
        raise NotImplementedError

    def retrieve_image(self, file_name: str) -> bytes:
        raise NotImplementedError

    def list_images(self) -> Generator[str, None, None]:
        raise NotImplementedError

    def save(self, stream: io.BytesIO, content_type: str) -> str:
        extension = mimetypes.guess_extension(content_type)
        image_id = uuid.uuid4()
        file_name = f'{image_id}{extension}'

        self.store_image(stream, file_name)

        return file_name


class LocalStorage(Storage):
    """
    Implements local disk filesystem storage.

    Goes easy for proof of concept and test purposes.
    """
    BASE_URI = config.STORAGE_BASE_URI

    def store_image(self, image_stream: io.BytesIO, file_name: str) -> None:
        image_path = os.path.join(self.BASE_URI, file_name)

        with open(image_path, 'wb') as image_file:
            while True:
                chunk = image_stream.read(self.CHUNK_SIZE_BYTES)
                if not chunk:
                    break

                image_file.write(chunk)

    def retrieve_image(self, file_name: str) -> Tuple[BinaryIO, int]:
        image_path = os.path.join(self.BASE_URI, file_name)

        # Check if file exists. Otherwise, user is requesting the image in a
        # different format than the one stored.
        if not os.path.exists(image_path):
            image_id, image_format = file_name.split('.')

            for _name in os.listdir(self.BASE_URI):
                if fnmatch.fnmatch(_name, f'{image_id}.*'):
                    image_path = os.path.join(self.BASE_URI, _name)
                    break
            else:
                # No image found with the given image ID. Image doesn't exist.
                raise FileNotFoundError

            # Open the image, transform it as user requested, save it on disk
            # for caching purposes and send.
            image = Image.open(image_path)
            new_image_path = os.path.join(
                self.BASE_URI, f'{image_id}.{image_format}'
            )
            with open(new_image_path, 'wb') as new_image:
                image.save(new_image, format=image_format)

            image_file = io.open(new_image_path, 'rb')
        else:
            image_file = io.open(image_path, 'rb')

        return image_file, os.path.getsize(image_path)

    def list_images(self) -> Generator[str, None, None]:
        for file_name in os.listdir(self.BASE_URI):
            yield file_name
