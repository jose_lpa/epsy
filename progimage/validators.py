from typing import Any

import falcon


ALLOWED_IMAGE_TYPES = (
    'image/bmp',
    'image/gif',
    'image/jpeg',
    'image/jpg',
    'image/png',
    'image/svg',
    # Good enough for the demo. A lot more formats could be added though.
)

ALLOWED_IMAGE_FORMATS = [
    image_type.split('/')[1] for image_type in ALLOWED_IMAGE_TYPES
]


def validate_image_type(
        request: falcon.Request,
        response: falcon.Response,
        resource: Any,
        params: dict
):
    if request.content_type not in ALLOWED_IMAGE_TYPES:
        raise falcon.HTTPBadRequest('Bad request', 'Image type not allowed.')


def validate_format(
        request: falcon.Request,
        response: falcon.Response,
        resource: Any,
        params: dict
):
    try:
        if request.path.split('.')[-1] not in ALLOWED_IMAGE_FORMATS:
            raise falcon.HTTPNotFound()
    except IndexError:
        raise falcon.HTTPNotFound()
