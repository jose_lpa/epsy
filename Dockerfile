FROM python:3.8.2-alpine3.11

LABEL maintainer="José L. Patiño-Andrés <jose.lpa@gmail.com>"


ENV TERM linux
ENV TERMINFO /etc/terminfo

# Install dependencies.
 RUN apk update && \
     apk add --virtual build-base gcc make python3-dev py-pip \
             musl-dev jpeg-dev zlib-dev && \
     rm -rf /var/cache/apk/*

# Set working directory.
RUN mkdir -p /opt/progimage
WORKDIR /opt/progimage

# Add app and install requirements.
COPY . /opt/progimage
RUN pip install -r requirements.txt

# Run application.
ENTRYPOINT ["gunicorn", "--reload", "progimage.app", "--bind", "0.0.0.0:8000", "--log-level", "DEBUG"]
