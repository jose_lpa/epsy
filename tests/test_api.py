import io
import json
import os
import uuid

import falcon

from PIL import Image

from progimage import config
from .conftest import client


def create_image() -> str:
    """ Helper function that creates dumb image on disk, for testing. """
    image_file_name = f'{uuid.uuid4()}.jpg'
    size = (100, 100)
    color = (255, 0, 0, 0)
    image = Image.new('RGB', size, color)
    image.save(
        os.path.join(config.STORAGE_BASE_URI, f'{image_file_name}'), 'JPEG'
    )

    return image_file_name


def test_list_images(client):
    """ Tests payload of images listing API endpoint. """
    image_file_name = create_image()

    payload = {'results': [image_file_name]}

    response = client.simulate_get('/images')
    result = json.loads(response.content)

    assert result == payload
    assert response.status == falcon.HTTP_OK


def test_image_post(client):
    """ Image is saved in backend storage after a successful POST request. """
    size = (100, 100)
    color = (255, 0, 0, 0)
    image = Image.new('RGB', size, color)

    with io.BytesIO() as image_file:
        image.save(image_file, format='JPEG')

        response = client.simulate_post(
            '/images',
            body=image_file.getvalue(),
            headers={'content-type': 'image/jpeg'}
        )

        assert response.status == falcon.HTTP_CREATED

        # Check that image file is present in disk storage.
        result = json.loads(response.content)
        assert os.path.exists(
            os.path.join(config.STORAGE_BASE_URI, result['image'])
        )


def test_image_post_invalid_format(client):
    """ Test API response when we post an invalid image type. """
    size = (100, 100)
    color = (255, 0, 0, 0)
    image = Image.new('RGB', size, color)

    with io.BytesIO() as image_file:
        # Let's use TIFF format for test, as it is deliberately not supported.
        image.save(image_file, format='TIFF')

        response = client.simulate_post(
            '/images',
            body=image_file.getvalue(),
            headers={'content-type': 'image/tiff'}
        )
        result = json.loads(response.content)

        assert response.status == falcon.HTTP_BAD_REQUEST
        assert result == {
            'title': 'Bad request',
            'description': 'Image type not allowed.'
        }


def test_image_detail(client):
    """ Test retrieving an image from the API endpoint. """
    image_file_name = create_image()

    response = client.simulate_get(f'/images/{image_file_name}')

    assert response.status == falcon.HTTP_OK


def test_image_detail_not_found(client):
    """ Test API response when image is not found or image type is not seen. """
    response = client.simulate_get(f'/images/{uuid.uuid4()}.jpg')

    assert response.status == falcon.HTTP_NOT_FOUND

    response = client.simulate_get(f'/images/INVALID_URL')

    assert response.status == falcon.HTTP_NOT_FOUND


def test_image_detail_select_format(client):
    """ Test retrieving an image in different format as the given one. """
    image_file_name = create_image()  # JPEG image created by default.

    # Make a request changing the `jpg` file extension by `png`.
    response = client.simulate_get(
        f'/images/{image_file_name.split(".")[0]}.png'
    )

    assert response.status == falcon.HTTP_OK
    assert response.headers.get('content-type') == 'image/png'
