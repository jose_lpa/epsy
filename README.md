# Epsy job interview assignment

## Run main microservice

The microservice can operate as a containerised solution by using Docker. This
is the preferred way of running the service. You will need 
[`docker-compose`](https://docs.docker.com/compose/), which you can install by
doing `pip install docker-compose`

When Docker and `docker-compose` are available in the system, just run:

    docker-compose build progimage
    docker-compose run progimage
    
And the service will be available at http://localhost/images.

To clean up environment after reviewing the assignment just do `Ctrl+C` and then:

    docker-compose down --rmi all
    
It is also possible to just run the assignment out of the box, without using any
containerised solution. If you don't want or can't use Docker in your machine,
simply run:

    pip install -r requirements.txt
    gunicorn --reload progimage.app --log-level DEBUG
    
And the service will be then available at http://localhost:8000/images

## Run unit tests

Unit tests can be run in Docker as well:

    docker-compose up progimage
    docker-compose exec progimage pytest tests
    
Or if you don't want to use Docker, and you installed requirements in your local:

    pytest test

## Questions

1. I used Python language, because the job position involves intensive usage of
   it and because it is the language I am more fluent and I know better. Another
   very good reason is the availability of highly performant and well tested
   libraries for Python language, such as [Pillow](https://python-pillow.org/)
   and [Falcon](https://falconframework.org/), which are very convenient for the
   job.
   
2. I stored the uploaded images directly in the server disk, and used no other
   ancillary service at all. The reason for doing this is that the service does
   not involve any other data handling than just storing the images and refer to
   them by the ID, which can directly be the file name itself using standard
   UUIDs. In case the task involved further data storage, such as "image title"
   or "image description" or any other image data rather than the image itself,
   I would have considered the usage of a database, which could have been a NoSQL
   database such as [CouchDB](https://couchdb.apache.org/) or 
   [MongoDB](https://www.mongodb.com/) or a SQL database such as
   [PostgreSQL](https://postgresql.org/) or [MariaDB](https://mariadb.org/), 
   depending on if the image data can be stored in a simple document or it needs 
   to be related to other sets of data, respectively.
   
   However, the disk storage was implemented in an abstract way, leaving room 
   for future requirements where for example a remote high performance storage
   solution could be needed, such as [AWS S3](https://aws.amazon.com/s3/).
   
3. To be honest, I would have done a better image transforming logic, for when
   the users retrieve an image using a different format suffix. I also would
   explore the possibility of using [gRPC](https://grpc.io/) in case the simple
   REST API approach is not proven sufficient to fulfill the demand, though I
   will leave this for internal APIs, and not for public ones.
   
4. I would set up CI/CD to coordinate the build and test process. Depending on
   business requirements and engineers preferences, there is a variety of
   available solutions to put such set ups in place. From simple private servers 
   running [Jenkins](https://jenkins.io/) to integrated commercial solutions.

5. Again, depending on the business needs and technical preferences, there is a
   wide range of solutions available to deploy and orchestrate containerised
   microservices to a production environment. If environment is keeping simple
   enough, a possibility is to use [Docker Swarm](https://docs.docker.com/engine/swarm/),
   which will be a fast and easy way of getting things done for production. If
   complexity of the microservices environment ramps up, more complete solutions
   can be used, such as [Kubernetes](https://kubernetes.io/), which clusters can
   be deployed in baremetal servers or in commercially available solutions from
   big providers like Amazon or Google. A big range of other solutions exists,
   like [AWS Fargate](https://aws.amazon.com/fargate/) or 
   [OpenShift](https://www.openshift.com/).
   
6. I would do automated testing, specifically I would do integration testing in
   this case because you make sure that the whole system is tested. For example
   I have tested the API endpoints for their responses, both with good and bad
   requests, so I can make sure the whole backend is working properly and
   returning meaningful data in both correct and incorrect user actions.
   
   Unit testing can also be considered in case the storage solution (this is,
   the `Storage` class and descendants) are going to be distributed in form of
   a library.